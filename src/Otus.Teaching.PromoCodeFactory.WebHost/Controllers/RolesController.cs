﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => 
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Удаление записи Role
        /// </summary>
        /// <param name="id">ИД записи Role</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteRoleByIdAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);
            if (role == null)
                return NotFound();
            await _rolesRepository.DeleteByIdAsync(id);
            return Ok();
        }

        /// <summary>
        /// Метод изменения записи Role
        /// </summary>
        /// <param name="id">ИД записи</param>
        /// <param name="name">эл. почта</param>
        /// <param name="description">кол-во промо</param>
        /// <returns></returns>
        [HttpPut("{id:guid}/{name}/{description}")]
        public async Task<ActionResult> UpdateRoleByIdAsync(Guid id, string name, string description)
        {
            var role = await _rolesRepository.GetByIdAsync(id);
            if (role == null)
                return NotFound();
            role.Name = name;
            role.Description = description;
            await _rolesRepository.UpdateByIdAsync(role);
            return Ok();
        }

        /// <summary>
        /// Создание новой записи Role
        /// </summary>
        /// <param name="role"></param>
        /// <returns>ИД созданной записи</returns>
        [HttpPost]
        public async Task<IActionResult> CreateRoleAsync([FromBody] Role role)
        {
            try
            {
                if (role == null)
                    return BadRequest();

                var emp = await _rolesRepository.GetByIdAsync(role.Id);
                if (emp != null)
                {
                    ModelState.AddModelError("Id", "Role already exists");
                    return BadRequest();
                }

                var createdRole = _rolesRepository.CreateAsync(role);

                return Ok(createdRole.Id);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error creating role");
            }
        }
    }
}