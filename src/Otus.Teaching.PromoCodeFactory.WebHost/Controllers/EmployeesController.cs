﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удаление записи Employee
        /// </summary>
        /// <param name="id">ИД записи Employee</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            await _employeeRepository.DeleteByIdAsync(id);
            return Ok();
        }

        /// <summary>
        /// Метод изменения записи Employee
        /// </summary>
        /// <param name="id">ИД записи</param>
        /// <param name="email">эл. почта</param>
        /// <param name="promoCount">кол-во промо</param>
        /// <returns></returns>
        [HttpPut("{id:guid}/{email}/{promoCount}")]
        public async Task<ActionResult> UpdateEmployeeByIdAsync(Guid id, string email, int promoCount)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            employee.Email = email;
            employee.AppliedPromocodesCount = promoCount;
            await _employeeRepository.UpdateByIdAsync(employee);
            return Ok();
        }

        /// <summary>
        /// Создание новой записи Employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns>ИД созданной записи</returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployeeAsync([FromBody]Employee employee)
        {
            try
            {
                if (employee == null)
                    return BadRequest();

                var emp = await _employeeRepository.GetByIdAsync(employee.Id);
                if (emp != null)
                {
                    ModelState.AddModelError("Id", "Employee already exists");
                    return BadRequest();
                }

                var createdEmployee = _employeeRepository.CreateAsync(employee);

                return Ok(createdEmployee.Id);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error creating employee");
            }
        }
    }
}