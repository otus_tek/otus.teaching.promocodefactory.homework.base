﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
       : IRepository<T>
       where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public Task DeleteByIdAsync(Guid id) 
        {
            var list = Data.Cast<T>().ToList();
            list.Remove(Data.FirstOrDefault(x => x.Id == id));
            Data = list.AsEnumerable<T>();
            return Task.FromResult(id);
        }

        public Task<T> UpdateByIdAsync(T value)
        {
            var t = Data.FirstOrDefault(x => x.Id == value.Id);
            t = value;
            return Task.FromResult(t);
        }

        public Task<T> CreateAsync(T value)
        {
            var list = Data.Cast<T>().ToList();
            list.Add(value);
            Data = list.AsEnumerable<T>();
            return Task.FromResult(value);
        }

    }
}